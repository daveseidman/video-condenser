import * as tf from '@tensorflow/tfjs';
import { createEl, addEl } from 'lmnt';
import autoBind from 'auto-bind';
import onChange from 'on-change';

import './index.scss';

const instructions = {
  warmup: 'Warming up...',
  classifying: 'Start by reviewing the first two minutes of the clip. As you watch, please touch P (play) on the keyboard to indicate a point has started, and I (idle) when the point has finished',
  collecting: 'The app is collecting frames',
  training: 'The model is now training based on your input',
  predicting: 'The model is now predicting',
};

const trainingDuration = 0.2 * 60;
const trainingFrequency = 0.1;

class App {
  constructor() {
    autoBind(this);

    const state = {
      step: null, // ['classifying', 'collecting', 'training']
      scrubbing: false,
      marker: 0,
      time: 0,
    };

    this.state = onChange(state, this.update);

    this.inputWidth = 224;
    this.inputHeight = 224;
    this.classes = ['playing', 'idle'];

    this.mobilenet = null;
    this.model = null;

    this.trainingDataInputs = [];
    this.trainingDataOutputs = [];
    this.examplesCount = [];
    this.videoMarkers = [];

    this.instructions = createEl('div', { className: 'instructions' });
    this.percent = createEl('div', { className: 'percent' });
    this.videoDiv = createEl('div', { className: 'video' });
    this.video = createEl('video', { className: 'video-player', src: 'assets/dodge-test-footage.mp4' }, {}, {
      loadedmetadata: (e) => {
        this.duration = this.video.duration;
      },
      click: this.playPause,
      timeupdate: this.timeUpdate,
    });
    this.timeline = createEl('div', { className: 'video-timeline' }, {}, { mousedown: this.mousedown, mouseleave: () => { this.state.scrubbing = false; }, mouseup: () => { this.state.scrubbing = true; } });
    this.progress = createEl('div', { className: 'video-timeline-progress' });
    this.markers = createEl('div', { className: 'video-timeline-markers' });
    addEl(this.timeline, this.progress, this.markers);

    this.graph = createEl('div', { className: 'video-graph' });
    this.bar1 = createEl('span', { className: 'video-graph-bar playing' });
    this.bar2 = createEl('span', { className: 'video-graph-bar idle' });
    addEl(this.graph, this.bar1, this.bar2);

    this.videoControls = createEl('div', { className: 'video-controls' });
    this.playPauseBtn = createEl('button', { className: 'video-controls-playpause' }, {}, { click: this.playpause });
    addEl(this.videoControls, this.playPauseBtn);
    addEl(this.videoDiv, this.video, this.timeline, this.videoControls, this.graph);

    // this.buttons = createEl('div', { className: 'class_buttons' });
    // this.trainButton = createEl('button', { innerText: 'train' }, {}, { click: this.train });
    // this.predictButton = createEl('button', { innerText: 'predict', className: 'disabled' }, {}, { click: this.predict });
    // this.saveButton = createEl('button', { innerText: 'save model' }, {}, { click: this.save });
    // this.classes.forEach((className, index) => {
    //   const button = createEl('button', { innerText: className }, { class: className, classId: index }, { click: this.collect, mouseup: () => { this.video.focus(); } });
    //   addEl(this.buttons, button);
    // });


    this.el = createEl('div', { className: 'app' });
    addEl(this.el);

    addEl(this.el, this.instructions, this.percent, this.videoDiv);// , this.buttons, this.trainButton, this.predictButton, this.saveButton);


    window.addEventListener('keydown', this.keydown);
    window.addEventListener('mousemove', this.mousemove);


    const URL = 'https://tfhub.dev/google/tfjs-model/imagenet/mobilenet_v3_small_100_224/feature_vector/5/default/1';
    tf.loadGraphModel(URL, { fromTFHub: true }).then((res) => {
      this.mobilenet = res;
      tf.tidy(() => {
        const answer = this.mobilenet.predict(tf.zeros([1, this.inputHeight, this.inputWidth, 3]));
        console.log(answer.shape);

        this.model = tf.sequential();
        this.model.add(tf.layers.dense({ inputShape: [1024], units: 128, activation: 'relu' }));
        this.model.add(tf.layers.dense({ units: this.classes.length, activation: 'softmax' }));

        this.model.summary();

        // Compile the model with the defined optimizer and specify a loss function to use.
        this.model.compile({
          optimizer: 'adam',
          loss: 'binaryCrossentropy',
          metrics: ['accuracy'],
        });
      });
      this.state.step = 'classifying';
    });

    this.state.step = 'warmup';

    // this.update();
  }

  update(path, current, previous) {
    console.log(`${path}: ${previous} -> ${current}`);

    if (path === 'step') {
      this.instructions.innerText = instructions[current];

      // if (current === 'collecting') {
      //   this.removeExtraMarkers();
      //   this.video.currentTime = 0;
      //   this.video.play();
      //   // this.collectFrame();
      // }

      if (current === 'training') {
        this.startTraining();
      }
    }
  }

  mousedown({ offsetX }) {
    this.state.scrubbing = true;
    const { width } = this.video.getBoundingClientRect();
    const time = (offsetX / width) * trainingDuration;
    this.video.currentTime = time;
    this.video.pause();
  }


  keydown({ code }) {
    if (!(code === 'KeyP' || code === 'KeyI')) return;

    const time = this.video.currentTime;
    const type = code === 'KeyP' ? 'playing' : 'idle';
    const el = createEl('span', { className: `video-timeline-markers-marker ${type}` }, {}, { mousedown: this.startDrag });
    el.style.left = `${(this.video.currentTime / trainingDuration) * 100}%`;
    this.videoMarkers.push({ time, type, el });

    addEl(this.markers, el);
  }

  mousemove({ offsetX }) {
    if (this.state.scrubbing) {
      const { width } = this.video.getBoundingClientRect();
      const time = (offsetX / width) * trainingDuration;
      this.video.currentTime = time;
      this.video.pause();
    }
  }

  timeUpdate() {
    const percent = (this.video.currentTime / trainingDuration) * 100;
    this.percent.innerText = `${Math.round(percent)}%`;


    this.progress.style.width = `${percent}%`;
    if (this.state.step === 'classifying') {
      if (this.video.currentTime >= trainingDuration) {
        // this.collectFrame();

        // this.video.removeEventListener('timeupdate', this.timeUpdate);
        const seeked = () => {
          this.state.step = 'collecting';
          this.video.removeEventListener('seeked', seeked);
        };

        // this.removeExtraMarkers();
        this.video.addEventListener('seeked', seeked);
        this.video.pause();
        this.video.currentTime = 0;
        this.video.play();
        // this.video.currentTime = 0;
        // this.video.play();
      }
    }


    if (this.state.step === 'collecting') {
      //   // done collecting
      if (this.video.currentTime >= trainingDuration) {
        this.state.step = 'training';
        return;
      }
      //
      //   // } else {
      let type;
      // not markers yet, use type from first marker
      if (this.video.currentTime < this.videoMarkers[0].time) {
        type = this.videoMarkers[0].type;
      } else {
        const nextMarker = this.videoMarkers.filter(marker => marker.time > this.video.currentTime)[0];
        // time is in between two markers, invert the next marker
        if (nextMarker) {
          type = nextMarker.type === 'playing' ? 'idle' : 'playing';
        } else {
          type = this.videoMarkers[this.videoMarkers.length - 1].type;
        }
      }
      const typeIndex = this.classes.indexOf(type);
      console.log(type, typeIndex);
      // console.log('collecting frame', this.video.currentTime, type);
      // this.video.removeEventListener('seeked', this.collectLoaded);
      const imageFeatures = tf.tidy(() => {
        const videoFrameAsTensor = tf.browser.fromPixels(this.video);
        const resizedTensorFrame = tf.image.resizeBilinear(videoFrameAsTensor, [this.inputHeight, this.inputWidth], true);
        const normalizedTensorFrame = resizedTensorFrame.div(255);
        return this.mobilenet.predict(normalizedTensorFrame.expandDims()).squeeze();
      });

      const marker = createEl('span', { className: `video-timeline-markers-marker small ${type}` });
      marker.style.left = `${(this.video.currentTime / trainingDuration) * 100}%`;
      addEl(this.markers, marker);

      // console.log(imageFeatures, type);
      this.trainingDataInputs.push(imageFeatures);
      this.trainingDataOutputs.push(typeIndex);
      // console.log(typeIndex);

      if (this.examplesCount[typeIndex] === undefined) {
        this.examplesCount[typeIndex] = 0;
      }
      this.examplesCount[typeIndex] += 1;
    }

    if (this.state.step === 'predicting') {
      this.predict();
    }
  }

  startDrag({ target }) {
    console.log('drag', target);
  }

  playPause() {
    this.video[this.video.paused ? 'play' : 'paused']();
  }

  removeExtraMarkers() {
    let type = null;
    this.videoMarkers.forEach((marker, index) => {
      if (marker.type === type) {
        this.videoMarkers[index].el.remove();
        this.videoMarkers.slice(index, 1);
      }
      type = marker.type;
    });
  }


  logProgress(e) {
    // console.log(e);
    this.percent.innerText = `${Math.round((e / 9) * 100)}%`;
  }

  startTraining() {
    // console.log('start training');
    tf.util.shuffleCombo(this.trainingDataInputs, this.trainingDataOutputs);
    const outputsAsTensor = tf.tensor1d(this.trainingDataOutputs, 'int32');
    const oneHotOutputs = tf.oneHot(outputsAsTensor, this.classes.length);
    const inputsAsTensor = tf.stack(this.trainingDataInputs);

    // let results = await
    this.model.fit(inputsAsTensor, oneHotOutputs, { shuffle: true, batchSize: 5, epochs: 10, callbacks: { onEpochEnd: this.logProgress } }).then((res) => {
      console.log('training complete', res);

      this.state.step = 'predicting';

      // this.predictButton.classList.remove('disabled');

      outputsAsTensor.dispose();
      oneHotOutputs.dispose();
      inputsAsTensor.dispose();
    });
  }

  predict() {
    console.log('predicting');
    tf.tidy(() => {
      const videoFrameAsTensor = tf.browser.fromPixels(this.video).div(255);
      const resizedTensorFrame = tf.image.resizeBilinear(videoFrameAsTensor, [this.inputHeight, this.inputWidth], true);

      const imageFeatures = this.mobilenet.predict(resizedTensorFrame.expandDims());
      const prediction = this.model.predict(imageFeatures).squeeze();
      const highestIndex = prediction.argMax().arraySync();
      const predictionArray = prediction.arraySync();

      console.log(highestIndex, predictionArray);
      this.bar1.style.height = `${predictionArray[0] * 100}%`;
      this.bar2.style.height = `${predictionArray[1] * 100}%`;

      // STATUS.innerText = `Prediction: ${classes[highestIndex]} with ${Math.floor(predictionArray[highestIndex] * 100)}% confidence`;
    });
  }


  save() {
    console.log('saving model');
    console.log(this.tf.save_model);
  }
}


const app = new App();
if (window.location.hostname === 'localhost') window.app = app;
